# Celephais supporting library

[[_TOC_]]

## Introduction
This library contains functions and variables used by Celephais to pull/remove OCI bundles, which are downloaded from a HTTP server using libcurl.

## Building and installing

### Docker container

You could install all tools needed for testing and developing on your local machine, but it is easier to just use a pre-configured environment. Such an environment is already prepared for you as a docker container.

1. Install docker

    Docker must be installed on your system.

    [Get Docker Engine for appropriate platform]( https://docs.docker.com/engine/install/)

    Make sure you user id is added to the docker group:

    ```bash
    sudo usermod -aG docker $USER
    ```

2. Fetch the container image

    To get access to the pre-configured environment, all you need to do is pull the image and launch a container.

    Pull the image:

    ```bash
    docker pull registry.gitlab.com/soft.at.home/docker/oss-dbg:latest
    ```

    Before launching the container, you should create a directory which will be shared between your local machine and the container.

    ```bash
    mkdir -p ~/workspace/lcm
    ```

3. Launch the container:

    ```bash
    docker run -ti -d --name oss-dbg --restart always --cap-add=SYS_PTRACE --sysctl net.ipv6.conf.all.disable_ipv6=1 -e "USER=$USER" -e "UID=$(id -u)" -e "GID=$(id -g)" -v ~/amx/:/home/$USER/workspace/lcm/ registry.gitlab.com/soft.at.home/docker/oss-dbg:latest
    ```

    The `-v` option bind mounts the local directory for the lcm project in the container, at the exact same place.
    The `-e` options create environment variables in the container. These variables are used to create a user name with exactly the same user id and group id in the container as on your local host (user mapping).

    You can open as many terminals/consoles as you like:

    ```bash
    docker exec -ti --user $USER oss-dbg /bin/bash
    ```

### Building

#### Prerequisites
- sudo apt update
- sudo apt install -y libcurl4-gnutls-dev libgpgme11-dev
- [libamxc](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxc)
- [libamxj](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxj)

#### Build and Install

1. Clone the git repository

    To be able to build it, you need the source code. So make a directory for the LCM and clone this library in it.

    ```bash
    mkdir -p ~/workspace/lcm/libraries
    cd ~/workspace/lcm/libraries
    git clone https://gitlab.com/prpl-foundation/lcm/libraries/libcelephais.git
    ```
2. Build it

    ```bash
    cd ~/workspace/lcm/libraries/libcelephais
    make
   ```

### Installing

#### Using make target install

You can install your own compiled version easily by running the install target.

```bash
cd ~/workspace/lcm/libraries/libcelephais
sudo make install
```
