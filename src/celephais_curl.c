/****************************************************************************
**
** Copyright (c) 2023 Consult Red
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/


#include <celephais_curl.h>
#include <amxc/amxc_string.h>
#include <celephais_defines.h>
#include <debug/sahtrace.h>

#define ME "celephais_curl"

static size_t write_curl_call_back_in_file(void* contents, size_t size, size_t nmemb, void* userp) 
{
    if(userp==NULL)
    {
        return -1;
    }

    FILE* fd = (FILE*) userp;
    fwrite(contents, size, nmemb, fd);

    return size * nmemb;
}

static celephais_status_t map_curlcode_to_celephaisstatus(CURLcode value) 
{
    celephais_status_t status = CELEPHAIS_NO_ERROR;
    switch(value) {
    case CURLE_OK:
        status = CELEPHAIS_NO_ERROR;
        break;
    case CURLE_OPERATION_TIMEDOUT:
        status = CELEPHAIS_ERROR_CURL_OPERATION_TIMEDOUT;
        break;
    case CURLE_COULDNT_RESOLVE_HOST:
        status = CELEPHAIS_ERROR_CURL_COULDNT_RESOLVE_HOST;
        break;
    default:
        status = CELEPHAIS_ERROR_DOWNLOAD_FAILED;
        break;
    }
    return status;
}

celephais_status_t celephais_curl_download_content(const celephais_curl_parameters_t* parameters, const amxc_string_t* url, void* data, curl_off_t* size, UNUSED bool head) 
{
    CURL* curl;
    CURLcode res = CURLE_OK;
    celephais_status_t status = CELEPHAIS_NO_ERROR;

    //auth
    amxc_string_t opt_token;
    struct curl_slist* list = NULL;

    curl_global_init(CURL_GLOBAL_ALL);
    curl = curl_easy_init();
    if( (parameters->type != FILE_TYPE_SIG) && (parameters->type != FILE_TYPE_TAR) ) 
    {
        SAH_TRACEZ_ERROR(ME,"celephais only supports curling tarballs & signatures");
        goto exit;
    }

    switch (parameters->auth_type) 
    {
    case BASIC_AUTHENTICATION:
        curl_easy_setopt(curl, CURLOPT_USERNAME, parameters->username->buffer);
        curl_easy_setopt(curl, CURLOPT_PASSWORD, parameters->password->buffer);
        break;
    case BEARER_AUTHENTICATION:
        amxc_string_init(&opt_token, 0);
        amxc_string_setf(&opt_token, "Authorization: Bearer %s", parameters->token->buffer);
        list = curl_slist_append(list, opt_token.buffer);
        amxc_string_clean(&opt_token);
        break;
    case NO_AUTHENTICATION:
    default:
        break;
    }

    curl_easy_setopt(curl, CURLOPT_URL, url->buffer);
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, list);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, data);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, (void*) write_curl_call_back_in_file);
    // follow redirection
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
    /* abort if slower than 30 bytes/sec during 10 seconds */
    curl_easy_setopt(curl, CURLOPT_LOW_SPEED_TIME, 10L);
    curl_easy_setopt(curl, CURLOPT_LOW_SPEED_LIMIT, 30L);

    SAH_TRACE_INFO("curl_easy_perform url: [%.64s...]", url->buffer);
    res = curl_easy_perform(curl);
    if(res != CURLE_OK) 
    {
        SAH_TRACEZ_ERROR(ME, "curl_easy_perform() failed: %s", curl_easy_strerror(res));
    }
    status = map_curlcode_to_celephaisstatus(res);

    curl_easy_getinfo(curl, CURLINFO_SIZE_DOWNLOAD_T, size);

    if(res == CURLE_OK)
    {   // ie invalid file on a valid webserver. 
        long response_code;
        curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);
        if(response_code != 200)
        {
            status = CELEPHAIS_ERROR_INVALID_ARGUMENTS;
        } 
    }
    
exit:
    curl_slist_free_all(list);
    curl_easy_cleanup(curl);
    curl_global_cleanup();
    return status;
}