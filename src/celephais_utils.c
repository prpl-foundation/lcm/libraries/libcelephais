/****************************************************************************
**
** Copyright (c) 2023 Consult Red
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <celephais_utils.h>
#include <celephais_defines.h>

#include <amxc/amxc.h>
#include <debug/sahtrace.h>

#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>

#define ME "celephais_utils"

void celephais_bundle_parameters_init(celephais_bundle_parameters_t* data)
{
    data->transport = NULL;
    data->bundle_name = NULL;
    data->version = NULL;
    amxc_string_init(&data->server, 0);
    data->auth_type = NO_AUTHENTICATION;
    amxc_string_init(&data->username, 0);
    amxc_string_init(&data->password, 0);
    amxc_string_init(&data->token, 0);
    data->signature_verification = false;
}
void celephais_bundle_parameters_clean(celephais_bundle_parameters_t* data)
{
    free(data->transport);
    free(data->bundle_name);
    free(data->version);
    amxc_string_clean(&data->server);
    amxc_string_clean(&data->username);
    amxc_string_clean(&data->password);
    amxc_string_clean(&data->token);
}

void celephais_parse_uri(UNUSED const char* uri, UNUSED celephais_bundle_parameters_t* uri_elements) {}

void celephais_parse_local_bundle(UNUSED const char* bundle_path,UNUSED celephais_bundle_parameters_t* path_parse) {}

bool check_bundle_validity(UNUSED const char * bundle_path)
{
    return false;
}

int celephais_hash_blob(UNUSED const char* filename,UNUSED char outputBuffer[65])
{
    return -1;
}

amxc_var_t* read_config(UNUSED const char* file)
{
    return NULL;
}

void celephais_split_path(char* path, amxc_llist_t* parts_out)
{
    amxc_string_t path_str;
    amxc_string_init(&path_str,0);
    amxc_string_push_buffer(&path_str, path, strlen(path)+1);
    
    amxc_string_split_to_llist(&path_str,parts_out,'/');
}

bool file_exists(const char* filename) {
    if(access(filename, F_OK) == 0) {
        return true;
    } else {
        return false;
    }
}

bool dir_exists(const char* dirname) 
{
    if(dirname==NULL)
    {
        return false;
    }

    struct stat s;
    if((stat(dirname, &s) != -1) && S_ISDIR(s.st_mode)) {
        return true;
    } else {
        return false;
    }
}

int celephais_create_storage_dir(char* path)
{
    amxc_llist_t parts;
    amxc_llist_init(&parts);
    celephais_split_path(path,&parts);
    //first element will be empty since the path starts with /. remove it
    amxc_llist_it_take(amxc_llist_get_first(&parts));
    
    size_t count = amxc_llist_size(&parts);
    if( count <= 0 ) return -1; //invalid path

    amxc_string_t pathbuilder;
    amxc_string_init(&pathbuilder,0);

    //temp string to store the ongoing path. 
    //amxc_string_setf seems to wipe its buffer before evaluating the format so it can't reference itself to update
    char* tmp = NULL;
    for(size_t i=0; i < count; i++)
    {
        if(i==0) 
        {
            amxc_string_setf(&pathbuilder,"/%s", amxc_string_get_from_llist(&parts,i)->buffer);
        }
        else 
        {
            amxc_string_setf(&pathbuilder, "%s/%s", tmp, amxc_string_get_from_llist(&parts,i)->buffer);
            free(tmp);
        }

        if(!dir_exists(pathbuilder.buffer))
        {
            if(mkdir(pathbuilder.buffer,0755) != 0) 
            {
                SAH_TRACEZ_ERROR(ME,"mkdir failed for %s !!",pathbuilder.buffer);
                return -1;
            }
        }

        if(i<count-1)
        {   //dont realloc tmp on the final pass
            tmp = strdup(pathbuilder.buffer);
        }
    }
    
    amxc_string_clean(&pathbuilder);
    amxc_llist_clean(&parts, amxc_string_list_it_free);
    return 0;
}