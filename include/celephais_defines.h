/****************************************************************************
**
** Copyright (c) 2023 Consult Red
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/


#ifndef _LIBCELEPHAIS_DEFINES_H_
#define _LIBCELEPHAIS_DEFINES_H_

#ifdef __cplusplus
extern "C"
{
#endif

#define UNUSED  __attribute((__unused__))
#define NONNULL __attribute((__nonnull__))

#define init_struct_of_chars(_struct , _data) \
{\
    void ** _data_start = (void**)(_data); \
    for (size_t i = 0; i < sizeof(_struct)/sizeof(char *); i ++) /*this only works as long as everything in the struct is a char*/\
    {\
        _data_start[i] = NULL;\
    }\
}

#define free_struct_of_chars(_struct , _data) \
{\
    void ** _data_start = (void**)(_data); \
    for (size_t i = 0; i < sizeof(_struct)/sizeof(char *); i ++) /*this only works as long as everything in the struct is a char*/\
    {\
        free(_data_start[i]); \
        _data_start[i] = NULL;\
    }\
}

#define CELEPHAIS "celephais"
#define CELEPHAIS_BUNDLES "Bundles"
#define CELEPHAIS_STORAGE_LOCATION "StorageLocation"
#define CELEPHAIS_BUNDLE_LOCATION "BundleLocation"
#define CELEPHAIS_SIGNATURE_VERIFICATION "SignatureVerification"

#define CELEPHAIS_DM CELEPHAIS
#define CELEPHAIS_DM_BUNDLES CELEPHAIS_DM "." CELEPHAIS_BUNDLES
#define CELEPHAIS_DM_STORAGE_LOCATION CELEPHAIS_DM "." CELEPHAIS_STORAGE_LOCATION
#define CELEPHAIS_DM_BUNDLE_LOCATION CELEPHAIS_DM "." CELEPHAIS_BUNDLE_LOCATION
#define CELEPHAIS_DM_SIGNATURE_VERIFICATION CELEPHAIS_DM "." CELEPHAIS_SIGNATURE_VERIFICATION

#define CELEPHAIS_CMD_PULL_URI  "URI"
#define CELEPHAIS_CMD_PULL_UUID  "UUID"
#define CELEPHAIS_CMD_PULL_DUID  "DUID"
#define CELEPHAIS_CMD_PULL_USERNAME "Username"
#define CELEPHAIS_CMD_PULL_PASSWORD "Password"

#define CELEPHAIS_CMD_UPDATE_URI  "URI"
#define CELEPHAIS_CMD_UPDATE_UUID  "UUID"
#define CELEPHAIS_CMD_UPDATE_DUID  "DUID"
#define CELEPHAIS_CMD_UPDATE_USERNAME "Username"
#define CELEPHAIS_CMD_UPDATE_PASSWORD "Password"

#define CELEPHAIS_CMD_REMOVE_DUID  "DUID"
#define CELEPHAIS_CMD_REMOVE_VERSION  "Version"

#define CELEPHAIS_DM_BUNDLE_DUID CELEPHAIS_CMD_PULL_DUID
#define CELEPHAIS_DM_BUNDLE_URI "URI"
#define CELEPHAIS_DM_BUNDLE_NAME "Name"
#define CELEPHAIS_DM_BUNDLE_VERSION "Version"
#define CELEPHAIS_DM_BUNDLE_VENDOR "Vendor"
#define CELEPHAIS_DM_BUNDLE_DESCRIPTION "Description"
#define CELEPHAIS_DM_BUNDLE_MARK_RM "MarkForRemoval"
#define CELEPHAIS_DM_BUNDLE_STATUS "Status"
#define CELEPHAIS_DM_BUNDLE_ERROR_CODE "ErrorCode"

#define CELEPHAIS_COMMAND "Command"
#define CELEPHAIS_COMMAND_ID "CommandID"
#define CELEPHAIS_COMMAND_PARAMETERS "Parameters"

#define CELEPHAIS_STATUS_DOWNLOADED "Downloaded"
#define CELEPHAIS_STATUS_DOWNLOADING "Downloading"
#define CELEPHAIS_STATUS_DOWNLOAD_FAILED "DownloadFailed"
#define CELEPHAIS_STATUS_UNENCRYPTION_FAILED "UnencryptionFailed"

#define CELEPHAIS_NOTIF_ERROR_TYPE "type"
#define CELEPHAIS_NOTIF_ERROR_TYPESTR CELEPHAIS_NOTIF_ERROR_TYPE "str"
#define CELEPHAIS_NOTIF_ERROR_COMMAND CELEPHAIS_COMMAND
#define CELEPHAIS_NOTIF_ERROR_REASON "reason"

#define CELEPHAIS_NOTIF_CONTAINER_ID "containerID"
#define CELEPHAIS_NOTIF_COMMAND_ID "commandID"
#define CELEPHAIS_NOTIF_PARAMETERS "parameters"
#define CELEPHAIS_NOTIF_BUNDLES "bundles"

#define CELEPHAIS_NOTIF_BUNDLE_PULLED "celephais:bundle:pulled"
#define CELEPHAIS_NOTIF_BUNDLE_MARKED_RM "celephais:bundle:markedremove"
#define CELEPHAIS_NOTIF_BUNDLE_REMOVED "celephais:bundle:removed"
#define CELEPHAIS_NOTIF_BUNDLE_STATUS "celephais:bundle:status"
#define CELEPHAIS_NOTIF_BUNDLE_LIST "celephais:bundle:list"
#define CELEPHAIS_NOTIF_ERROR "celephais:bundle:error"

#define CELEPHAIS_CMD_COMMAND "command"

#define CELEPHAIS_CMD_RM_PARAMKEY "paramKey"
#define CELEPHAIS_CMD_RM_PARAMVALUE "paramValue"

#define CELEPHAIS_CMD_PULL "pull"
#define CELEPHAIS_CMD_UPDATE "update"
#define CELEPHAIS_CMD_REMOVE "remove"
#define CELEPHAIS_CMD_GC "garbage_collection"

#define CELEPHAIS_CMD_SV "signature_verification"
#define CELEPHAIS_CMD_SV_ENABLE "Enable"

#ifdef __cplusplus
}
#endif


#endif