/****************************************************************************
**
** Copyright (c) 2023 Consult Red
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/


#ifndef _LIBCELEPHAIS_CURL_H_
#define _LIBCELEPHAIS_CURL_H_

#include <celephais_auth.h>
#include <celephais_status.h>
#include <curl/curl.h>
#include <stdbool.h>

typedef struct _amxc_string amxc_string_t;

typedef enum _celephais_curl_file_type {
    FILE_TYPE_MANIFEST  = 0,
    FILE_TYPE_CONFIG  = 1,
    FILE_TYPE_BLOB = 2,
    FILE_TYPE_SIG = 3,
    FILE_TYPE_TAR = 4,
} celephais_curl_file_type_t;

typedef struct _celephais_curl_parameters {
    const amxc_string_t* username;
    const amxc_string_t* password;
    const amxc_string_t* token;
    const celephais_curl_file_type_t type;
    const celephais_auth_type_t auth_type;
} celephais_curl_parameters_t;

/// @brief download content
/// @param parameters a pointer to the curl parameters to use to make the download
/// @param url a pointer to the URL to work with
/// @param data pointer to be passed to curl's write callback , stores the download (and headers if enabled)
/// @param size receives the size of the download in bytes (excludes header size if enabled)
/// @param head if true sets curl so that it returns headers as well as content
/// @return celephais_status_t status code depending on download result
celephais_status_t celephais_curl_download_content(const celephais_curl_parameters_t* parameters, const amxc_string_t* url, void* data, curl_off_t* size, bool head);

#endif