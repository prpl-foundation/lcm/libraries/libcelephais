/****************************************************************************
**
** Copyright (c) 2023 Consult Red
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/


#ifndef _LIBCELEPHAIS_UTILS_H_
#define _LIBCELEPHAIS_UTILS_H_

#include <celephais_auth.h>
#include <celephais_defines.h>

#include <amxc/amxc_string.h>
#include <amxc/amxc_variant.h>

typedef struct _celephais_bundle_parameters {
    char* transport;
    char* bundle_name;
    char* version;
    amxc_string_t server;
    celephais_auth_type_t auth_type;
    amxc_string_t username;
    amxc_string_t password;
    amxc_string_t token;
    bool signature_verification;
} celephais_bundle_parameters_t;

/// @brief  initialize the fields in the celephais_bundle_parameters_t struct
/// @param data struct to initialize
NONNULL void celephais_bundle_parameters_init(celephais_bundle_parameters_t* data);

/// @brief  free the fields in the celephais_bundle_parameters_t struct
/// @param data struct to cleanup
NONNULL void celephais_bundle_parameters_clean(celephais_bundle_parameters_t* data);

/// @brief parse a URI to create a bundle parameters struct
/// @param uri URI to parse
/// @param uri_elements takes in a celephais_bundle_parameters_t pointer and sets it based on the URI 
void celephais_parse_uri(const char* uri, celephais_bundle_parameters_t* uri_elements);

/// @brief parse (an already downloaded) local bundle to get the bundle parameters
/// @param bundle_path path to the bundle to parse 
/// @param path_parse takes in a celephais_bundle_parameters_t pointer and sets it based info in files at the bundle_path 
void celephais_parse_local_bundle(const char* bundle_path, celephais_bundle_parameters_t* path_parse);

/// @brief check that a bundle appears valid according to the oci spec
/// @param bundle_path path of bundle to check
/// @return true if valid, false if invalid
bool check_bundle_validity(UNUSED const char * bundle_path);

/// @brief generates a sha256 hash of a file
/// @param filename file to generate hash of
/// @param outputBuffer buffer to put hash into (with null terminated char at end)
/// @return 0 on success, -1 on error
int celephais_hash_blob(const char* filename, char outputBuffer[65]);

/// @brief reads a OCI config json and return it as an amxc_var_t
/// @param file filename of file to be read
/// @return returns a amxc_var_t containing the json file on success, and NULL on failure
amxc_var_t* read_config(const char* file);

/// @brief splits a filesystem path by each /
/// @param path the path to split
/// @param parts_out pointer to an initialised amxc_llist_t which will recieve the split parts
void celephais_split_path(char* path, amxc_llist_t* parts_out);

/// @brief checks the existence of a file
/// @param filename path to file to check
/// @return true if the file exists, false if not
bool file_exists(const char* filename);

/// @brief checks the existence of a directory
/// @param dirname path to directory to check
/// @return true if the dir exists, false if not
bool dir_exists(const char* dirname);

/// @brief creates the bundle storage dir given a llist from celephais
/// @param path full directory path which must be created. Will create all directories in the path
/// @return 0 on success, -1 otherwise
int celephais_create_storage_dir(char* path);

#endif