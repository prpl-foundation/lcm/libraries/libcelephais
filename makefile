include makefile.inc

.PHONY: all clean
#TODO add phony test once there are tests

all clean:
	$(MAKE) -C src $@

install: all
	$(INSTALL) -d -m 0755 $(DEST)/$(INCLUDEDIR)/celephais
	$(INSTALL) -D -p -m 0644 include/*.h $(DEST)$(INCLUDEDIR)/celephais/
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so.$(VERSION) $(DEST)$(LIBDIR)/$(COMPONENT).so.$(VERSION)
	ln -sfr $(DEST)$(LIBDIR)/$(COMPONENT).so.$(VERSION) $(DEST)$(LIBDIR)/$(COMPONENT).so.$(VMAJOR)
	ln -sfr $(DEST)$(LIBDIR)/$(COMPONENT).so.$(VERSION) $(DEST)$(LIBDIR)/$(COMPONENT).so
